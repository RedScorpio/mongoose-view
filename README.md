# mongoose-view

Allows to create views of any kind (i.e. json objects for api made with node.js) from Mongoose models.

## Views

### Requires

To use the View utility in any source file, simply require:

		var View = require('mongoose-view')

You can also use express middleware that will attach useful functions to res:

		require('mongoose-view').middleware(app);

### Registering a View

By default the utility supports Mongoose models and strings passed as names. View can be registered only once. You can create multiple View flavors of one model. Each View flavor is supposed to have it's own creator.

		var User = mongoose.model('User', UserSchema);
		View.register(User, 'Simple', function(viewer, callback){
			var json = {
				id: this.id,
				displayName: this.displayName,
				profileImageUrl: this.profileImageUrl
			};
			callback(null, json);
		});

**View.register(Model, flavor, creator)** - Registers a View creator. One model can have multiple view variations, determined by flavors.

* Model - Mongoose model or it's name
* flavor - Optional flavor of creator.
* creator(viewer, callback) - Function that renders the view.

If you're curious, you can check which View creators are currently registered:

		console.log(View.creators);

### Rendering a View

View can be rendered directly in a controller or inside other View.

		function(req, res){
			res.view(req.service, 'Simple', req.user, (err, serviceView)=>{
				if(err)...
				res.json(serviceView);
			});
		}

The above can be simplified to:

		function(req, res){
			res.view(req.service, 'Simple');
		}

If you need to render a View inside other View:

		View.register(..., function(viewer, callback){
			var json = {
				id: this.id
			}
			View.render(this.service, 'Enhanced', viewer, (err, serviceView)=>{
				if(err)...
				json.service = serviceView;
			})
		});

**res.view(model, flavor, viewer, callback)** - View function attached to response object. Allows to create a view by passing it's model.

* model - Model from which the view will be created. Must be present. It also has to have registered View. Can be a single object or an array.
* flavor - String representing variation of View.
* viewer - User that will see generated view. It equals to req.user by default.
* callback(err, views) - A callback that will return views (or a single view). If not present, a view will be returned using res.json(views).

**View.render(model, flavor, viewer, callback)** - View function attached to response object. Allows to create a view by passing it's model.

* model - Model from which the view will be created. Must be present. It also has to have registered View
* flavor - String representing variation of View.
* viewer - User that will see generated view. It equals to req.user by default.
* callback(err, views) - A callback that will return views (or a single view). If not present, a view will be returned using res.json(views).

## Pagination

Paginated view includes three properties:

* list - actual list of views (or any objects)
* count - total amount items that can be returned using the query
* pages - amount of pages that can be returned with current `displayLimit`

It is only bound to `res` object after middleware is attached.

		function(req, res){
			res.pagination(User, {displayName: 'John'}, (findCall, end)=>{
				findCall.exec((err, users)=>{
					if(err)...
					res.view(users, end);
				});
			});
		}

**res.pagination(Model, flavor, findQuery, callback)** - Generates paginated views from a Mongoose Model, find query and optional callback. It takes `displayLimit` and `displayPage` from `req.query` and viewer from `req.user`.

* Model - Mongoose object.
* flavor - Optional view variation.
* findQuery - Optional Mongoose find query.
* callback(findCall, end) - Optional callback. If not present, paginated views are returned via res.json(). `findCall` is a initially prepared Model.find(..) function that has proper limit and offset. `end` is a callback that should be called in order to render paginated view. It's arguments are `(err, views)`.

## Errors

### If No Error

Returns an error message to the user if an error occurred. Continues by calling a call with argument passed by standard callback.

		function(req, res){
			someAsyncCall(res.ifNoError(result=>{
				console.log(result);
				...
			}));
		}

**res.ifNoError(call)**

* call - Function to be called on success.
* returns Function - Wrapped call function that is called only in case of success.

### Error

A quick way to return an error message without using ifNoError or any other utils.

		function(req, res){
			if(!req.body.key){
				return res.error('Missing key parameter!');
			}
			...
		}

**res.error(err, code)** - Returns an error message by calling res.status(code).json(...).

* err - An Error or a string describing the error.
* code - Optional http code number. By default error message code is used. In case of no code present in `err` it falls back to 400.
