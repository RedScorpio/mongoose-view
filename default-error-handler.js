'use strict';

/**
 * Get unique error field name
 */
var getUniqueErrorMessage = function (err) {
  var output;

  try {
    var fieldName = err.errmsg.substring(err.errmsg.lastIndexOf('.$') + 2, err.errmsg.lastIndexOf('_1'));
    output = fieldName.charAt(0).toUpperCase() + fieldName.slice(1) + ' already exists';

  } catch (ex) {
    output = 'Unique field already exists';
  }

  return output;
};

/**
 * Get the error message from error object
 */
function getErrorMessage(err) {
  var message = 'Something went wrong';

  if (err.code) {
    switch (err.code) {
      case 11000:
      case 11001:
        message = getUniqueErrorMessage(err);
        break;
    }
  } else {
    message = '';
    var errors = err.errors;
    for (var errName in errors) {
      if (errors.hasOwnProperty(errName) && errors[errName].message) {
        message += errors[errName].message;
      }
    }
  }

  return message;
}

var environment = process.env.NODE_ENV;
if (environment === 'test' || environment === 'development') {
  exports.getErrorMessage = function (err) {
    var message = getErrorMessage(err);
    return message === '' ? err.toString() : message;
  };
} else {
  exports.getErrorMessage = getErrorMessage;
}
