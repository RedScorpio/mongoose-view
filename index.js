'use strict';

var async = require('async');

const DEFAULT_ELEMENTS_LIMIT = 10;

/**
 * Creates a new view from mongoose model (i.e. result of find call).
 *
 * @param {String?} flavor - optional view type
 * @param model - Mongoose model
 * @constructor
 */
var View = function (flavor, model) {
  if (!model) {
    model = flavor;
    flavor = null;
  }
  var creatorName;
  this.noElements = false;
  if (Array.isArray(model)) {
    this.multipleElements = true;
    if (model.length > 0) {
      if (model[0].constructor) {
        creatorName = model[0].constructor.modelName;
      }
    } else {
      this.noElements = true;
    }
  } else if (model.constructor) {
    this.multipleElements = false;
    creatorName = model.constructor.modelName;
  }
  if (!this.noElements && (!creatorName || typeof creatorName !== 'string')) {
    throw new Error('Specified model is not a valid mongoose model or array of models: ' + model);
  }
  this.model = model;
  if (flavor) {
    creatorName = creatorName + ':' + flavor;
  }
  this.creator = View.creators[creatorName];
  if (!this.noElements && !this.creator) {
    throw new Error('View creator not found for ' + creatorName + '. Please check if you registered such view.');
  }
};
View.creators = [];

/**
 * Registers a View creator. One model can have multiple view variations, determined by flavors.
 * @param Model - Mongoose Model or it's name.
 * @param {String?} flavor - Optional flavor of creator.
 * @param {function(viewer:Object, callback:function(err:Error?, view:Object))} creator - Function that renders the view.
 */
View.register = function (Model, flavor, creator) {
  if (!creator) {
    //noinspection JSValidateTypes
    creator = flavor;
    flavor = null;
  }
  if (!Model) {
    throw new Error('Argument "Model" is missing.');
  }
  if (typeof Model !== 'string') {
    Model = Model.modelName;
  }
  if (!Model || typeof Model !== 'string') {
    throw new Error('Passed "Model" is not a valid mongoose model. Expected name or result of `mongoose.model(..)`, got ' + JSON.stringify(Model, null, 2));
  }
  if (!creator) {
    throw new Error('Argument "creator" is missing.');
  }
  if (typeof creator !== 'function') {
    throw new Error('View creator is not a function.');
  }
  if (creator.length !== 2) {
    throw new Error('View creator is expected to have exactly two arguments: viewer and callback.');
  }
  var creatorName = flavor ? (Model + ':' + flavor) : Model;
  if (View.creators[creatorName]) {
    throw new Error('View creator named ' + creatorName + ' already exists. Please use different name for this View.');
  }
  View.creators[creatorName] = creator;
};
/**
 * Renders a View.
 *
 * @param viewer User that will see rendered view.
 * @param {function(err:Error?, view:{}?)} callback View is returned via callback
 */
View.prototype.show = function (viewer, callback) {
  if (!callback) {
    callback = viewer;
    viewer = null;
  }

  var creator = this.creator;
  if (this.multipleElements) {
    if (this.noElements) {
      return callback(null, []);
    }
    async.map(this.model, function (model, next) {
      if (model) {
        return creator.bind(model)(viewer, next);
      }
      next();
    }, function (err, views) {
      if (err) {
        return callback(err);
      }
      callback(null, views.filter(function (view) {
        return !!view;
      }));
    });
  } else {
    creator.bind(this.model)(viewer, callback);
  }
};

View.paginatedView = function(req, res, flavor, viewer, model, findQuery, callback) {
  var limit = DEFAULT_ELEMENTS_LIMIT, page = 0;

  if (req.query.displayPage) {
    page = parseInt(req.query.displayPage);
    if (isNaN(page)) {
      return res.error('Page must be a numeric value.');
    } else if (page < 0) {
      return res.error('Page can not be less or equal 0.');
    }
  }
  if (req.query.displayLimit) {
    limit = parseInt(req.query.displayLimit);
    if (isNaN(limit)) {
      return res.error('Limit must be a numeric value.');
    } else if (limit < 1) {
      return res.error('Limit can not be less or equal 0.');
    }
  }
  if (flavor && typeof flavor !== 'string') {
    callback = findQuery;
    findQuery = model;
    model = viewer;
    viewer = flavor;
    flavor = null;
  }
  if (!callback) {
    callback = function (findCall, done) {
      findCall.exec(function (err, elements) {
        if (err) {
          return done(err);
        }
        new View(flavor, elements).show(viewer, done);
      });

    };
  }
  model.count(findQuery).exec(function (err, count) {
    if (err) {
      return res.error(err);
    }
    var findCall = model.find(findQuery).skip(page * limit).limit(limit);
    callback(findCall, function (err, list) {
      if (err) {
        return res.error(err);
      }
      res.json({ list: list, count: count, pages: Math.ceil(count / limit) });
    });
  });
}

/**
 * View function attached to response object. Allows to create a view by passing it's model.
 *
 * @param {Object} model - Model from which the view will be created. Must be present. It also has to have registered View
 * @param {String?} flavor - String representing variation of View.
 * @param {Object} viewer - User that will see generated view. It equals to req.user by default.
 * @param {function(err:Error?,views:*)} callback - A callback that will return views (or a single view). If not present, a view will be returned using res.json(views).
 */
View.render = function (model, flavor, viewer, callback) {
  if (!model) {
    throw new Error('Model is missing.');
  }
  if (typeof flavor !== 'string') {
    //noinspection JSValidateTypes
    callback = viewer;
    viewer = flavor;
    flavor = null;
  }
  if (typeof viewer === 'function') {
    //noinspection JSValidateTypes
    callback = viewer;
    viewer = null;
  }
  if (!callback) {
    throw new Error('View.render: Missing callback.');
  }
  new View(flavor, model).show(viewer, callback);
};

/**
 *
 * @param {object} app Express app to which middleware is attached
 * @param {object?} errorHandler Interface to get errors
 * @param {function(err:Error|String)} errorHandler.getErrorMessage Returns error message from error
 */
View.middleware = function (app, errorHandler) {
  if (!errorHandler) {
    errorHandler = require('./default-error-handler')
  }
  app.use(function (req, res, next) {
    /**
     * Returns an error message to the user if an error occurred.
     * Continues by calling a call with argument passed by standard callback.
     *
     * @param {Function} call - Function to be called on success.
     * @returns {Function} Wrapped call function that is called only in case of success.
     */
    res.ifNoError = function (call) {
      return function (err, result) {
        if (err) {
          return res.status(err.statusCode || 400).send({
            message: errorHandler.getErrorMessage(err)
          });
        }
        call(result);
      };
    };

    /**
     * Returns an error message by calling res.status(code).json(...).
     *
     * @param {Error|String} err - An error or a string describing the error.
     * @param {Number?} code - Optional http code. By default error message code is used. In case of no code present in err it falls back to 400.
     * @returns {*}
     */
    res.error = function (err, code) {
      if (err instanceof Error) {
        err = errorHandler.getErrorMessage(err);
      }
      return res.status(code || err.code || 400).json({ message: err });
    };

    /**
     * View function attached to response object. Allows to create a view by passing it's model.
     * @param model - Model from which the view will be created. Must be present. It also has to have registered View. Can be a single object or an array.
     * @param {String?} flavor - String representing variation of View.
     * @param {?} viewer - User that will see generated view. It equals to req.user by default.
     * @param {function(err:Error?,views:[]|{}?)?} callback - A callback that will return views (or a single view). If not present, a view will be returned using res.json(views).
     */
    res.view = function (model, flavor, viewer, callback) {
      if (!model) {
        throw new Error('Model is missing.');
      }
      if (typeof flavor !== 'string') {
        callback = viewer;
        viewer = flavor;
        flavor = null;
      }
      if (typeof viewer === 'function') {
        callback = viewer;
        viewer = null;
      }
      if (!viewer) {
        viewer = req.user;
      }
      if (!callback) {
        callback = res.ifNoError(views => {
          res.json(views);
        });
      }
      new View(flavor, model).show(viewer, callback);
    };

    /**
     * Generates paginated views from a Mongoose Model, find query and optional callback.
     * It takes displayLimit and displayPage from req.query and viewer from req.user.
     *
     * @param Model - Mongoose object.
     * @param {String?} flavor - Optional view variation.
     * @param {Object?} findQuery - Optional Mongoose find query.
     * @param {Function?} callback - Optional callback. If not present, paginated views are returned via res.json().
     */
    res.pagination = function (Model, flavor, findQuery, callback) {
      var viewer = req.user;
      if (typeof flavor !== 'string') {
        //noinspection JSValidateTypes
        callback = findQuery;
        findQuery = flavor;
        flavor = null;
      }
      if (typeof findQuery !== 'object') {
        //noinspection JSValidateTypes
        callback = findQuery;
        findQuery = null;
      }
      View.paginatedView(req, res, flavor, viewer, Model, findQuery, callback);
    };
    next();
  });
};

module.exports = View;
